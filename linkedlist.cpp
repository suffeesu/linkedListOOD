#include <iostream>
#include <cstdlib>

class Node
{
	
	private:
	
		int data;
		Node *next;	
	
	public	:
		Node()
		{
			this->data = 0;
			this->next = NULL;
		}
	
		Node(int value)
		{
			this->data = value;
			this->next = NULL;
		}	
	
		void setNext(Node* ptr)
		{		
			this->next = ptr;
		}
	
		void setData(int value)
		{
			this->data = value;
		}
	
		Node* getNext()
		{
			return this->next;		
		}
	
		int getData()
		{
			return this->data;	
		}

};

class list{
		
	private:	
	
		Node* first;
		int mounts;
		
	public:
		
		list()
		{
			this->first = NULL;
			this->mounts = 0;	
		} 
		
		
		bool isNotInitial(){
			return this->first==NULL;
		}
		
		bool isEmpty(){
			return this->mounts==0;
		}	
		
		
		bool add(int item){
			
			if(isNotInitial())
			{
				first = new Node(item);			
			}
			else
			{
				Node *temp = first;
				
				while(temp->getNext()!=NULL)
					temp= temp->getNext();	
				
				temp->setNext(new Node(item)) ;
			}
			
			this->mounts++;
			
			return true;
		}
		
		bool del(int value){
			
			if(isEmpty())
				return false;
				
			Node *temp=this->first;
			Node *front=NULL;		
			
			if(this->first->getData()==value)
			{
				first= first->getNext();
				delete(temp);	
			}	
			else
			{
				while(temp->getData()!=value)
				{
					front=temp;
					temp= temp->getNext();
					if(temp==NULL)
						return false;	
				}
			
				front->setNext(temp->getNext());
				delete(temp);
			}
			
			this->mounts--;
			return true;
		}
		
		void print()
		{
			Node *temp=this->first;
			
			while(temp!=NULL)	
			{
				printf("%d-->",temp->getData());
				temp = temp->getNext();
			}
			
			printf("NULL\n");
		}
};

int main(void){

	list *a = new list();


	for(int i=0;i<10;i++)
		a->add(i);
		
	a->print();

	
	for(int i=3;i<15;i++)
		a->del(i);
	
	a->print();

}
